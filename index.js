/* eslint-disable no-undef */
'use strict';
require('dotenv').config();
require('express-async-errors');
const express = require('express');
const cors = require('cors');
const app = express();
const PORT = process.env.PORT || 3001;
const { log } = require('./src/helper/logger');
const databaseConnection = require('./src/db/db');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.listen(PORT, async () => {
	console.log(`
    ###########################################
     🚀 Cron Server's listening on port: ${PORT} 🚀
    ###########################################
    `);
	try {
		const db = await databaseConnection();
		require('./app')(db);
	} catch (error) {
		console.log(error);
		log('error', 'Database not connected');
	}
});
