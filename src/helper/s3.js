
const AWS = require('aws-sdk');
const fs = require('fs');
const path = require('path');

const s3 = new AWS.S3(
	{
		accessKeyId: process.env.S3_ACCESS_KEY,
		secretAccessKey: process.env.S3_SECRET_KEY,
		region: 'us-east-1',
		signatureVersion: 'v4'
	},
);

async function S3upload(params) {
	return new Promise((resolve, reject) => {
		s3.upload(params, function (err, data) {
			if (err) {
				reject(err);
			}
			resolve(data.key);
		});
	});
}
//upload a file to s3
module.exports.uploadFile = async (file, fileName) => {
	// let fileName = file.name.replace(/ /g, '');
	// eslint-disable-next-line no-useless-escape
	// fileName = fileName.replace(/[&\/\\#,+()$~%'":*?<>{}]/g, '_');

	const readFile =  fs.readFileSync('test.xlsx');
	const uploadParams = {
		Bucket: process.env.S3_BUCKET_NAME,
		Body: readFile,
		Key: fileName,
		ContentEncoding: 'base64',
		//ACL: 'public-read'
	};

	await S3upload(uploadParams);
	return fileName;
};

module.exports.readFile = (filename) => {
	const options = {
		Bucket: process.env.S3_BUCKET_NAME,
		Key: filename
	};

	const fileStream = s3.getObject(options);
	return fileStream.createReadStream();
};

// function deletedFile() {
// 	const params = {
// 		Bucket: config.S3_BUCKET_NAME,
// 		Key: 'WhatsApp Image 2022-01-14 at 10.15.12 AM.jpeg',
// 	};
// 	s3.deleteObject(params, function (err, suc) {
// 		if (err) {
// 			console.log(err);
// 		}
// 		console.log('File Deleted');
// 	});
// }
// deletedFile('WhatsApp Image 2022 - 01 - 14 at 10.15.12 AM.jpeg')

module.exports.getSignedURL = (filename) => {
	const signedUrlExpireSeconds = 60 * 5; // your expiry time in seconds.
	return s3.getSignedUrl('getObject', {
		Bucket: process.env.S3_BUCKET_NAME,
		Key: filename,
		Expires: signedUrlExpireSeconds
	});
	
};

// S3_ACCESS_KEY = AKIAQWIIFKLLVZLJGD6L
// S3_SECRET_KEY = uoAIGpTxj755aJl7QW1n6DlW9pTIf1a + ZoCHKSZ0
// S3_BUCKET_NAME = aza - bridge - reports