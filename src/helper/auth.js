/* eslint-disable no-unused-vars */
'use strict';
const { log } = require('./logger');
const { generateJwtToken, validateJwtToken } = require('./jwt');

module.exports = async function auth(req, res, next) {
	log('info', 'auth initiated', 'auth');
	const token = req.header('x-auth-token');
	
	if (!token) return res.status(401).send('Access denied.No token provided.');
	try {
		const decode_payload = await validateJwtToken(token);
		if (!decode_payload.success) {
			res.status(400).send('Invalid token.');
		} else {
			req.userData = { data: decode_payload.data };
			
			next();
		}
	} catch (error) {
		res.status(400).send('Invalid token.');
	}
};
