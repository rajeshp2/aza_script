
module.exports.getName = async (tableName, id) => {
	const name = await tableName.findAll({ where: { addedBy: id } });
	return name;
};
module.exports.getValue = async (tableName, object, attributesName) => {
	const name = await tableName.findOne({ where: object, attributes: [attributesName]});
	return name;
};