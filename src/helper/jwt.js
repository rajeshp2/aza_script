/* eslint-disable no-unused-vars */
'use strict';
const jwt = require('jsonwebtoken');
const { error } = require('winston');

exports.generateJwtToken = (data) => {
	return jwt.sign({ data: data }, process.env.JWT_SECRET_KEY, { expiresIn: '1d' }); // Here Expirey is set to 1 day
	
};

exports.validateJwtToken = (token) => {
	return new Promise((resolve, reject) => {
		jwt.verify(token, process.env.JWT_SECRET_KEY, function (err, decoded) {
		
			if (err) {
				reject({
					success: false,
					error: err
				});
			} else {
				resolve({
					success: true,
					data: decoded
				});
			}
		});
	});
};
