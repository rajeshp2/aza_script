exports.time = () => {
	var currentDate = new Date();
	var time = {};
	time.date = currentDate.getDate();
	time.month = currentDate.getMonth(); //Be careful! January is 0 not 1
	time.year = currentDate.getFullYear();
	time.hour = currentDate.getHours();
	time.minute = currentDate.getMinutes();
	time.second = currentDate.getSeconds();

	return `${time.year}-${time.month + 1}-${time.date} ${time.hour}:${time.minute}:${time.second}`;
};