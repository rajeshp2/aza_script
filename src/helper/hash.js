'use strict';
var crypto = require('crypto');

exports.setPassword = (password) => {
	let salt = crypto.randomBytes(16).toString('hex');
	let hash = crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex');
	return ({
		hash: hash,
		salt: salt
	});
};

exports.validatePassword = (password, encrypted_hash, encrypted_hash_salt) => {
	var hash = crypto.pbkdf2Sync(password, encrypted_hash_salt, 1000, 64, 'sha512').toString('hex');
	return hash === encrypted_hash;
};
