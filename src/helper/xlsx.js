/* eslint-disable no-unused-vars */
const XLSX = require('xlsx');
const path = require('path');
const fs = require('fs');

const convertJsonToExcel = async (data, pageName) => {
	const filename = `${pageName.replace(' ', '_')}${new Date().toISOString()}.xlsx`;
	const filepath = path.join(__dirname, '..', 'download', filename);
	const workSheet = XLSX.utils.json_to_sheet(data);
	const workBook = XLSX.utils.book_new();
	XLSX.utils.book_append_sheet(workBook, workSheet, 'admin');
	XLSX.writeFile(workBook, 'test.xlsx');
	return {filepath, filename};
};

module.exports = {
	convertJsonToExcel
};