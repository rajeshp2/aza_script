const parseHTMLBody = (rawBody, data) => {
	const keys = Object.keys(data);
	let processedBody = rawBody;
	keys.forEach(key => {
		const keyWord = `{{${key}}}`;
		processedBody = processedBody.replaceAll(keyWord, data[key]);
	});
	return processedBody;
};

module.exports = { parseHTMLBody };