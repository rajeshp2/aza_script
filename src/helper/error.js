'use strict';
const { error } = require('./logger');

// eslint-disable-next-line no-unused-vars
module.exports = function (err, req, res, next) {
	// Log the exception and return a friendly error to the client.
	error(`${err.message}:${err}`);
	res.status(500).send('Something went wrong !! .......');
};
