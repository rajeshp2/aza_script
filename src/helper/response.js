'use strict';
exports.resSuccess = (message, data, statusCode) => {
	return {
		message,
		error: false,
		status: statusCode,
		data
	};
};

exports.resError = (message, errorStack, statusCode) => {
	// List of common HTTP request code
	// eslint-disable-next-line no-unused-vars
	const codes = [200, 201, 400, 401, 404, 403, 409, 422, 500];

	//Get matched code
	const findCode = codes.find((code) => code == statusCode);

	if (!findCode) statusCode = 500;
	else statusCode = findCode;

	return {
		message,
		error: true,
		status: statusCode,
		errorStack
	};
};

exports.resValidation = (errors) => {
	return {
		message: 'Validation error',
		error: true,
		status: 400,
		errors
	};
};
