'use strict';

module.exports = {
	port: process.env.PORT || 3000,
	sql: {
		username: process.env.SQL_USERNAME,
		password: process.env.SQL_PASSWORD,
		database: process.env.SQL_DATABASE,
	}
};