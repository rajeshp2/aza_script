'use strict';

if ((process.env.NODE_ENV && process.env.NODE_ENV === 'local') ||
    !process.env.NODE_ENV) {
	const dotenv = require('dotenv');
	const result = dotenv.config();
	if (result.error) {
		throw result.error;
	}
}

module.exports = {
	mailTemplates: {
		curateInactivationMail: {
			subject: 'Curate Inactivation Reminder Mail',
			body: `<p>{{ curatesTitle }} created by {{ addedBy }} will be inactivated on {{ manualInactivationDate }}.
				After inactivation, customers will be redirected to homepage of the website if they click on this curate link.Please make sure that{{ curatesTitle }} 
				is not currently in use and extend inactivation date if needed.
                <br/>
                </p>
                <h3>Sincerely,<br/>
                AZA <br/>
                Development Team</h3>`
		
		},
		passwordReset: {
			subject: 'Password reset for AZA',
			body: `<p>You are receiving this because you (or someone else) have requested the reset of the password for your account.<br/><br/>

                Please click on the following link, or paste this into your browser to complete the process:<br/><br/>
               {{link}}
                <br/>
                If you did not request this, please ignore this email and your password will remain unchanged.\n 
                the above link will automatically expire after 10 minutes </p>
                <h3>Sincerely,<br/>
                AZA <br/>
                Development Team</h3>`
		},
	}
};