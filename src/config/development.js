'use strict';

if ((process.env.NODE_ENV && process.env.NODE_ENV === 'local') ||
!process.env.NODE_ENV) {
	const dotenv = require('dotenv');
	const result = dotenv.config();
	if (result.error) {
		throw result.error;
	}
}

module.exports = {
	port: process.env.PORT || 3000,
	sql: {
		username: process.env.SQL_USERNAME,
		password: process.env.SQL_PASSWORD,
		database: process.env.SQL_DATABASE,
		host: process.env.SQL_HOST,
		port: process.env.SQL_PORT,
		dialect: process.env.SQL_DIALECT,
		models: {
			tbl_admin_user: 'tbl_admin_user',
			tbl_admin_role: 'tbl_admin_role',
			tbl_admin_menu: 'tbl_admin_menu',
			tbl_admin_user_access: 'tbl_admin_user_access',
			tbl_pg_table_mapping: 'tbl_pg_table_mapping',
			tbl_reset_password: 'tbl_reset_password',
			tbl_role_categories: 'tbl_role_categories',
			tbl_logs_curate_inactivation: 'tbl_logs_curate_inactivation',
			mx_color: 'mx_color',
			mx_size: 'mx_size',
			mx_category:'category',
			tbl_curates:'tbl_curates',
			mx_product:'mx_product',
			tbl_product_curates:'mx_product_curates',
			mx_product_set: 'mx_product_set',
			mx_designer: 'mx_designer'
		},
		folder:{
			tbl_admin_user: 'user',
			tbl_admin_role: 'user',
			tbl_admin_menu: 'user',
			tbl_admin_user_access: 'user',
			tbl_pg_table_mapping: 'user',
			tbl_reset_password: 'user',
			tbl_role_categories: 'user',
			tbl_logs_curate_inactivation: 'logs',
			mx_color: 'master',
			mx_size: 'master',
			mx_category: 'master',
			tbl_curates:'curate',
			mx_product:'product',
			tbl_product_curates:'product',
			mx_product_set: 'product',
			mx_designer:'designer'
		}
	}
};