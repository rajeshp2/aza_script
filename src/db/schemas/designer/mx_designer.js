'use strict';

const {  DataTypes } = require('sequelize');
const { sql } = require('../../../config');

/**
 * Generate user Model
 * @param {Sequelize} sequelize sequelize client object
 */

module.exports = (sequelize) => {

	const mxDesignerModel = sql.models.mx_designer;

	// Define the schema
	const mxDesigner = sequelize.define(mxDesignerModel, {
		designerId: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false
		},
		designerName: {
			type: DataTypes.STRING,
			allowNull: false
		},
		
	},
	{ timestamps: false, updatedAt: false, createdAt: false }
	);

	// Syncing the model with the DB creates the table if doesn't exists (and does nothing if it already exists)
	return mxDesigner.sync();
};