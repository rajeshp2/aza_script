'use strict';

const { Sequelize, DataTypes } = require('sequelize');
const { sql } = require('../../../config');

/**
 * Generate user Model
 * @param {Sequelize} sequelize sequelize client object
 */

module.exports = (sequelize) => {
	const logsCurateInactivationModel = sql.models.tbl_logs_curate_inactivation;

	// Define the schema
	const logsCurateInactivation = sequelize.define(logsCurateInactivationModel, {
		id: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false
		},
		curatesId: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		inactivationDate: {
			type: DataTypes.STRING,
			allowNull: false
		},
		inActivatedBy: {
			type: DataTypes.STRING,
			allowNull: true
		},
		dateAdded: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		}
	},
	{ timestamps: false, updatedAt: false, createdAt: false });

	// Syncing the model with the DB creates the table if doesn't exists (and does nothing if it already exists)
	return logsCurateInactivation.sync();
};