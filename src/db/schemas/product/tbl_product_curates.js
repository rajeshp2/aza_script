'use strict';

const { Sequelize, DataTypes } = require('sequelize');
const { sql } = require('../../../config');

/**
 * Generate user Model
 * @param {Sequelize} sequelize sequelize client object
 */

module.exports = (sequelize) => {

	const tblProductCuratesModel = sql.models.tbl_product_curates;

	// Define the schema
	const tblProductCurates = sequelize.define(tblProductCuratesModel, {
		id: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false
		},
		product_id: {
			field: 'productId',
			type: DataTypes.INTEGER,
			allowNull: false
		},
		curates_id: {
			field: 'curatesId',
			type: DataTypes.INTEGER,
			allowNull: false
		},
		status: {
			type: DataTypes.TINYINT,
			allowNull: false,
			defaultValue: 1
		},
		date_added: {
			field: 'dateAdded',
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
	},
	{ timestamps: false, updatedAt: false, createdAt: false }
	);

	// Syncing the model with the DB creates the table if doesn't exists (and does nothing if it already exists)
	return tblProductCurates.sync();
};