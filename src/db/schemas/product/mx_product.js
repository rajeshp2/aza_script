'use strict';

const { Sequelize, DataTypes } = require('sequelize');
const { sql } = require('../../../config');

/**
 * Generate user Model
 * @param {Sequelize} sequelize sequelize client object
 */

module.exports = (sequelize) => {

	const mxProductModel = sql.models.mx_product;

	// Define the schema
	const mxProduct = sequelize.define(mxProductModel, {
		productId: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false
		},
		categoryId: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		subCategoryId: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		designerId: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		productTitle: {
			type: DataTypes.STRING,
			allowNull: false
		},
		colorId: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		stylistNote: {
			type: DataTypes.STRING,
			allowNull: true
		},
		status: {
			type: DataTypes.TINYINT,
			allowNull: false,
			defaultValue: 1
		},
		addedBy: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		modifiedBy: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		dateAdded: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
		dateModified: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
	},
	{ timestamps: false, updatedAt: false, createdAt: false }
	);

	// Syncing the model with the DB creates the table if doesn't exists (and does nothing if it already exists)
	return mxProduct.sync();
};