'use strict';

const { DataTypes } = require('sequelize');
const { sql } = require('../../../config');

/**
 * Generate user Model
 * @param {Sequelize} sequelize sequelize client object
 */

module.exports = (sequelize) => {

	const mxProductSetModel = sql.models.mx_product_set;

	// Define the schema
	const mxProductSet = sequelize.define(mxProductSetModel, {
		productSetId: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false
		},
		productId: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		sizeId: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		productPrice: {
			type: DataTypes.FLOAT,
			allowNull: false
		},
		discountPrice: {
			type: DataTypes.FLOAT,
			allowNull: false
		},
	},
	{ timestamps: false, updatedAt: false, createdAt: false }
	);

	// Syncing the model with the DB creates the table if doesn't exists (and does nothing if it already exists)
	return mxProductSet.sync();
};