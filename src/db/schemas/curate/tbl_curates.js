'use strict';

const { Sequelize, DataTypes } = require('sequelize');
const { sql } = require('../../../config');

/**
 * Generate user Model
 * @param {Sequelize} sequelize sequelize client object
 */

module.exports = (sequelize) => {

	const tblCuratesModel = sql.models.tbl_curates;

	// Define the schema
	const tblCurates = sequelize.define(tblCuratesModel, {
		id: {
			field: 'id',
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false
		},
		curates_title: {
			field: 'curatesTitle',
			type: DataTypes.STRING,
			allowNull: false
		},
		curates_description: {
			field: 'curatesDescription',
			type: DataTypes.STRING,
			allowNull: true
		},
		is_searchable: {
			field: 'isSearchable',
			type: DataTypes.TINYINT,
			allowNull: false,
			defaultValue: 1
		},
		is_deleted: {
			field: 'isDeleted',
			type: DataTypes.TINYINT,
			allowNull: false
		},
		status: {
			field: 'status',
			type: DataTypes.TINYINT,
			allowNull: false,
			defaultValue: 1
		},
		status_inactive_date: {
			field: 'statusInactiveDate',
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
		manual_inactivation_date: {
			field: 'manualInactivationDate',
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: false
		},
		curate_inactivated_by: {
			field: 'curateInactivatedBy',
			type: DataTypes.STRING,
			allowNull: true
		},
		is_show_timer: {
			field: 'isShowTimer',
			type: DataTypes.TINYINT,
			allowNull: false,
			defaultValue: 0
		},
		timer_start_date: {
			field: 'timerStartDate',
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
		timer_end_date: {
			field: 'timerEndDate',
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
		is_wave_off_duty: {
			field: 'isWaveOffDuty',
			type: DataTypes.TINYINT,
			allowNull: false,
			defaultValue: 0
		},
		default_sort_action: {
			field: 'defaultSortAction',
			type: DataTypes.STRING,
			allowNull: true
		},
		added_by: {
			field: 'addedBy',
			type: DataTypes.STRING,
			allowNull: true
		},
		modified_by: {
			field: 'modifiedBy',
			type: DataTypes.BIGINT,
			allowNull: true
		},
		date_added: {
			field: 'dateAdded',
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
		date_modified: {
			field: 'dateModified',
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
	},
	{ timestamps: false, updatedAt: false, createdAt: false }
	);

	// Syncing the model with the DB creates the table if doesn't exists (and does nothing if it already exists)
	return tblCurates.sync();
};