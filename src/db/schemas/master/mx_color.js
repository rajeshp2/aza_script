'use strict';

const { Sequelize, DataTypes } = require('sequelize');
const { sql } = require('../../../config');

/**
 * Generate user Model
 * @param {Sequelize} sequelize sequelize client object
 */

module.exports = (sequelize) => {

	const mxColorModel = sql.models.mx_color;

	// Define the schema
	const mxColor = sequelize.define(mxColorModel, {
		colorId: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false
		},
		colorName: {
			type: DataTypes.STRING,
			allowNull: false
		},
		colorCode: {
			type: DataTypes.STRING,
			allowNull: true
		},
		colorAlias: {
			type: DataTypes.STRING,
			allowNull: true
		},
		level: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		parentId: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		status: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		isDeleted: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		addedBy: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		modifiedBy: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		dateAdded: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
		dateModified: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
	},
	{timestamps: false, updatedAt: false, createdAt: false }
	);

	// Syncing the model with the DB creates the table if doesn't exists (and does nothing if it already exists)
	return mxColor.sync();
};