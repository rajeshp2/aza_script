'use strict';

const { Sequelize, DataTypes } = require('sequelize');
const { sql } = require('../../../config');

/**
 * Generate user Model
 * @param {Sequelize} sequelize sequelize client object
 */

module.exports = (sequelize) => {

	const mxSizeModel = sql.models.mx_size;

	// Define the schema
	const mxSize = sequelize.define(mxSizeModel, {
		sizeId: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false
		},
		sizeTitle: {
			type: DataTypes.STRING,
			allowNull: false
		},
		sizeClass: {
			type: DataTypes.STRING,
			allowNull: false
		},
		level: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		parentId: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		dispOrder: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		status: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		isDeleted: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		addedBy: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		modifiedBy: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		dateAdded: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
		dateModified: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
	},
	{ timestamps: false, updatedAt: false, createdAt: false }
	);

	// Syncing the model with the DB creates the table if doesn't exists (and does nothing if it already exists)
	return mxSize.sync();
};