'use strict';

const { Sequelize, DataTypes } = require('sequelize');
const { sql } = require('../../../config');

/**
 * Generate user Model
 * @param {Sequelize} sequelize sequelize client object
 */

module.exports = (sequelize) => {

	const categoryModel = sql.models.mx_category;

	// Define the schema
	const mx_category = sequelize.define(categoryModel, {
	
		categoryId: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false
		},
		categoryTitle: {
			type: DataTypes.STRING,
			allowNull: false
		},
		categoryGst: {
			type: DataTypes.INTEGER,
			allowNull: true
		},

		seoUri: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		categorySeoContent: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		imageName: {
			type: DataTypes.STRING,
			allowNull: true
		},
		level: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		parentId: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		xOrder: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		subCatDispOrder: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		displayOrder: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		status: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		addedBy: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		modifiedBy: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		dateAdded: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
		dateModified: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
		isDeleted: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		
	
	},{timestamps: false, updatedAt: false, createdAt: false});

	// Syncing the model with the DB creates the table if doesn't exists (and does nothing if it already exists)
	return mx_category.sync();
};