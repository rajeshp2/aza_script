'use strict';
const { Sequelize, DataTypes } = require('sequelize');
const { sql } = require('../../config');

/**
 * Generate user Model
 * @param {Sequelize} sequelize sequelize client object
 */

module.exports = (sequelize) => {

    const colorModel = sql.models.color;

    // Define the schema
    const color = sequelize.define(colorModel, {
        colorId: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        colorName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        colorCode: {
            type: DataTypes.STRING,
            allowNull: true
        },
        level: {
            type: DataTypes.TINYINT,
            allowNull: true
        },
        parentId: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        status: {
            type: DataTypes.TINYINT,
            allowNull: true
        },
        addedBy: {
            type: DataTypes.BIGINT,
            allowNull: false
        },
        modifiedBy: {
            type: DataTypes.BIGINT,
            allowNull: true
        },
        dateAdded: {
            type: DataTypes.DATE,
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: true
        },
        dateModified: {
            type: DataTypes.DATE,
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: true
        },
    });

    // Syncing the model with the DB creates the table if doesn't exists (and does nothing if it already exists)
    return color.sync();
};
