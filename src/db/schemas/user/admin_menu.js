'use strict';

const { Sequelize, DataTypes } = require('sequelize');
const { sql } = require('../../../config');

/**
 * Generate user Model
 * @param {Sequelize} sequelize sequelize client object
 */

module.exports = (sequelize) => {
	const adminMenuModel = sql.models.admin_menu;

	// Define the schema
	const adminMenu = sequelize.define(adminMenuModel, {
		id: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false
		},
		menuType: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		menuTitle: {
			type: DataTypes.STRING,
			allowNull: false
		},
		parentId: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		uri: {
			type: DataTypes.STRING,
			allowNull: true
		},
		status: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		isDeleted: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		addedBy: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		modifiedBy: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		dateAdded: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
		dateModified: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
	},
	{ timestamps: false, updatedAt: false, createdAt: false });

	// Syncing the model with the DB creates the table if doesn't exists (and does nothing if it already exists)
	return adminMenu.sync();
};