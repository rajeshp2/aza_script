'use strict';

const { Sequelize, DataTypes } = require('sequelize');
const { sql } = require('../../../config');

/**
 * Generate user Model
 * @param {Sequelize} sequelize sequelize client object
 */

module.exports = (sequelize) => {
	const adminUserAccessModel = sql.models.tbl_admin_user_access;

	// Define the schema
	const adminUserAccess = sequelize.define(adminUserAccessModel, {
		id: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false
		},
		roleAID: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		adminMenuId: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		view: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		add: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		edit: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		delete: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		export: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		status: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		addedBy: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		modifiedBy: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		dateAdded: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
		dateModified: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
		isDeleted: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		
	});

	// Syncing the model with the DB creates the table if doesn't exists (and does nothing if it already exists)
	return adminUserAccess.sync();
};