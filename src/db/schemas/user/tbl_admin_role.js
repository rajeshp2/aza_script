'use strict';

const { Sequelize, DataTypes } = require('sequelize');
const { sql } = require('../../../config');

/**
 * Generate userTypeModel Model
 * @param {Sequelize} sequelize sequelize client object
 */

module.exports = (sequelize) => {
	const adminUserRoleModel = sql.models.tbl_admin_role;
	// Define the schema
	const userType = sequelize.define(adminUserRoleModel, {
        
		id: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false
		},
		categoryId: {
			type: DataTypes.BIGINT,
			allowNull: false
		},

		roleName: {
			type: DataTypes.STRING,
			allowNull: false
		},
		status: {
			type: DataTypes.BOOLEAN,
			allowNull: true
		},
		addedBy: {
			type: DataTypes.STRING,
			allowNull: true
		},
     
		modifiedBy: {
			type: DataTypes.STRING,
			allowNull: true
            
		},
		dateAdded: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: false
		},
		dateModified: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: false
		},
		isDeleted: {
			type: DataTypes.BOOLEAN,
			allowNull: true
		},
	},{
		timestamps: false
	});

	// Syncing the model with the DB creates the table if doesn't exists (and does nothing if it already exists)
	return userType.sync();
};