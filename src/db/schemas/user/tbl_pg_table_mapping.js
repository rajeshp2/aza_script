'use strict';

const { Sequelize, DataTypes } = require('sequelize');
const { sql } = require('../../../config');

/**
 * Generate user Model
 * @param {Sequelize} sequelize sequelize client object
 */

module.exports = (sequelize) => {
	const pgTableMappingModel = sql.models.tbl_pg_table_mapping;

	// Define the schema
	const pgTableMapping = sequelize.define(pgTableMappingModel, {
		id: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false
		},
		pageName: {
			type: DataTypes.STRING,
			allowNull: false
		},
		tableName: {
			type: DataTypes.STRING,
			allowNull: false
		},
		status: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		isDeleted: {
			type: DataTypes.TINYINT,
			allowNull: true
		},
		addedBy: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		modifiedBy: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		dateAdded: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
		dateModified: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			allowNull: true
		},
		primaryKey: {
			type: DataTypes.STRING,
			allowNull: false
		}
	},
	{ timestamps: false, updatedAt: false, createdAt: false }
	);

	// Syncing the model with the DB creates the table if doesn't exists (and does nothing if it already exists)
	return pgTableMapping.sync();
};