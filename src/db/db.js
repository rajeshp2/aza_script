'use strict';

const { Sequelize } = require('sequelize');
const path = require('path');
const directory = path.join(__dirname, 'schemas');

module.exports = async () => {
	const sequelize = await sequelizeConnect();

	await generateModels(sequelize);

	return sequelize;
};

const sequelizeConnect = async () => {
	const { sql } = require('../config');
	const options = {
		host: sql.host,
		port: sql.port,
		dialect: sql.dialect,
		define: {
			freezeTableName: true
		},
		logging: console.log
	};

	const sequelize = new Sequelize(sql.database, sql.username, sql.password, options);

	try {
		await sequelize.authenticate();
		console.log(`Connection to database ${sql.database} has been established successfully`);
		return sequelize;
	} catch (error) {
		console.log('Unable to connect to database', error);
		return error;
	}
};

const generateModels = async(sequelize) =>{
	const { sql } = require('../config');
	const models = Object.keys(sql.models);
	const folders = Object.keys(sql.folder);
	
	for (let i = 0; i < models.length; i++) {
		// eslint-disable-next-line no-undef
		const filePath = `${directory}/${sql.folder[folders[i]]}/${sql.models[models[i]]}.js`;
		await require(filePath)(sequelize);
	}

};
