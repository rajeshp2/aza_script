const config = require('../../config');
const { sendMail } = require('../../helper/sendMail');
const cron = require('node-cron');
const moment = require('moment');
const models = config.sql.models;
const Sequelize = require('sequelize');
//const common_send_mail = require('../../common/send-mail');

//const { selectOnCondition, selectOnWhereIn, selectFrom3JoinedTable } = require('../../services/common.service');

// cron for reminder at 9.00 am 1 day before session
module.exports = async (db) => {
	cron.schedule('*/1 * * * *', async () => {
		try {
			const dateAfter90Days = moment().add(1, 'days').format('YYYY-MM-DD');
			console.log(dateAfter90Days);
			const tbl_logs_curate_inactivation = db.models[models.tbl_logs_curate_inactivation];
			const tbl_curates = db.models[models.tbl_curates];
			const tbl_product_curates = db.models[models.tbl_product_curates];
			const tbl_admin_user = db.models[models.tbl_admin_user];
			const curateProductdata = await tbl_curates.findAll({ where: { status_inactive_date: new Date(dateAfter90Days) },  attributes: ['id'], raw:true});
			console.log(curateProductdata);

			curateProductdata.forEach(async curatesId => {
				//const id = await mx_product_curates.destroy({
				//     where: {
				//         id: curatesId.id
				//     }
				// });
				const id = await tbl_product_curates.findAll({
					where: {
						id: curatesId.id
					}
				});
				console.log(id);
			});

			//const dateAfter13Months = moment().add(1, 'days').format('YYYY/MM/DD');
         
			//const curatesdata = await tbl_curates.findAll({ where: { status_inactive_date: dateAfter13Months }, attributes: ['id'], raw: true });
			
			//curatesdata.forEach(async curatesId => {
				
			//const id = await mx_product_curates.destroy({
			//     where: {
			//         id: curatesId.id
			//     }
			// });
			//	const id = await tbl_curates.findAll({
			//	where: {
			//			id: curatesId.id
			//		}
			//	});
			//	console.log(id);
			//});

			// const dateAfter3days = moment().add(3, 'days').format('YYYY/MM/DD');

			// const curatesManualInactivationdata = await tbl_curates.findAll({ where: { manual_inactivation_date: dateAfter3days }, attributes: ['id', 'curates_title', 'added_by', 'manual_inactivation_date'], raw: true });

			// curatesManualInactivationdata.forEach(async curatesId => {
				
			// 	const id = await tbl_admin_user.findAll({
			// 		where: {fullName: curatesId.added_by},attributes: ['email'], raw: true 
			// 	});
			// 	const user = {
			// 		curates_title: curatesId.curatesTitle ,
			// 		added_by: curatesId.addedBy, 
			// 		manual_inactivation_date: curatesId.manual_inactivation_date,
			// 		email :id.email
			// 	};

			// 	const mailResponseData = await sendMail(user, 'curateInactivationMail');
			// });

			// const todaysDate = moment().format('YYYY/MM/DD');

			// const curatesManualInactivation = await tbl_curates.findAll({ where: { manualInactivationDate: todaysDate }, attributes: ['id', 'status','added_by','curate_inactivated_by'], raw: true });

			// curatesManualInactivation.forEach(async curatesId => {
			// 	const updateObject = {
			// 		status: 0
			// 	};
			// 	const changeCurateStatus = await tbl_curates.update(updateObject, { where: { curatesId: curatesId.curatesId }, raw: true });
				
			// 	const logData = {
			// 		curates_id: curatesId.curates_id,
			// 		added_by: curatesId.added_by,
			// 		inactivation_date: curatesId.manual_inactivation_date,
			// 		inactivated_by: curatesId.curate_inactivated_by,
			// 		date_added: new Date()
			// 	};

			// 	const addLogData = await tbl_logs_curate_inactivation.create(logData, { where: { id: curatesId.id }});
			// });




			// const sessionData = await selectFrom3JoinedTable
			// (config.tableNames.SESSIONS, config.tableNames.CLIENTS, config.tableNames.PROFESSIONALS,
			// 	'client_id', 'client_id', 'professional_id', 'professional_id', '=',
			// 	[`${config.tableNames.SESSIONS}.*`,
			// 		`${config.tableNames.CLIENTS}.first_name as clientName`,
			// 		`${config.tableNames.CLIENTS}.email as clientEmail`,
			// 		`${config.tableNames.CLIENTS}.client_id as clientId`,
			// 		`${config.tableNames.CLIENTS}.first_name as clientFirstName`,
			// 		`${config.tableNames.CLIENTS}.last_name as clientLastName`,
			// 		`${config.tableNames.PROFESSIONALS}.first_name as professionalFirstName`,
			// 		`${config.tableNames.PROFESSIONALS}.last_name as professionalLastName`],
			// 	1,
			// 	'session_date', '=', tomorrow);

			// sessionData.forEach(async data => {
			// 	let mailResponseData;
			// 	mailResponseData = await sendMail(
			// 		{
			// 			clientName: data.clientFirstName + ' ' + data.clientLastName,
			// 			email: data.clientEmail,
			// 			type: data.type == 1 ? 'Trial' : 'Regular',
			// 			sessionTime: data.session_start_time,
			// 			sessionWith: data.professionalFirstName + ' ' + data.professionalLastName,
			// 			sessionLink: `${config.FRONTEND_LOCAL_URL}/video-call/${data.session_uuid}`
			// 		}, 'ONE_DAY_SESSION_REMINDER');
			// 	const mailLogData = {
			// 		user_type: 1,
			// 		user_id: data.clientId,
			// 		request_type: 1,
			// 		request_data: JSON.stringify(mailResponseData.request),
			// 		response_data: JSON.stringify(mailResponseData.response),
			// 		created_at: new Date(),
			// 		updated_at: new Date(),
			// 	};
			// 	await common_service.insert(config.tableNames.EMAIL_SMS_LOGS, mailLogData);
			// 	debug(`Mail sent to ${data.clientEmail}`);

			// // common_send_mail.sendMail(data, 'reminderAtNineAmOneDayBefore');
			// });
		} catch (error) {
			console.log(error);
			console.log('Error in reminder cron at 9.00 am 1 day before session', error.stack);
		}
	});};



	